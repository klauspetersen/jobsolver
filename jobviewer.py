with open('C:/temp/myfile.txt') as f:
    lines = [line.rstrip().split(";") for line in f]

jobs = {}
for line in lines:
    name = line[0]
    pulseStart = line[1]
    pulseEnd = line[2]
    gateStart = line[3]
    gateEnd = line[4]
    if not jobs.has_key(name):
        jobs[name] = list()

    jobs[name].append(line[:])

bottom = 0
for key in jobs.keys():
    lefts = []
    widths = []
    colors = []
    bottoms = []
    bottom = bottom + 1
    for job in jobs[key]:
        lefts.append(int(job[1]))
        widths.append(int(job[2])-int(job[1]))
        colors.append("r")
        bottoms.append(bottom)
        lefts.append(int(job[3]))
        widths.append(int(job[4])-int(job[3]))
        colors.append("b")
        bottoms.append(bottom)

    plt.bar(left=np.array(lefts), height=1, width=np.array(widths), bottom=np.array(bottoms), color=np.array(colors), orientation="horizontal")

plt.show()