/*
 * jobsolver.cpp
 *
 *  Created on: 30/10/2014
 *      Author: kpp
 */
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <stdint.h>
#include <vector>
#include <assert.h>
#include <limits.h>

#define JOBSOLVER_BASEBOARD_NUM_CHAN 32
#define JOBSOLVER_MAX_NUM_CONSTRAINTS 32
#define JOBSOLVER_PREALLOCATED_JOBS 8

#define CLK_FREQ 100000000.0
#define CLK_PR_SEC 1.0 / CLK_FREQ

#define MS_PR_SEC 1000.0
#define US_PR_SEC 1000000.0
#define NS_PR_SEC 1000000000.0

typedef int32_t local_time_t;
typedef int64_t system_time_t;

/* Convert us to clock cycles */
#define us_to_cc(time) (local_time_t)(((time) / US_PR_SEC) / (CLK_PR_SEC))
#define ns_to_cc(time) (local_time_t)(((time) / NS_PR_SEC) / (CLK_PR_SEC))

FILE * pFile;

typedef enum {
	lth,
	gth
} constriant_type_t;



struct Channel{
	Channel(){
		timeSinceLastPulse = 0;
	}
	local_time_t timeSinceLastPulse;
};

struct Adc{
	Adc(){
		timeSinceLastGate = 0;
		timeToNextGateStart = 0;
		timeToNextGateEnd = 0;
	}
	local_time_t timeSinceLastGate;
	local_time_t timeToNextGateStart;
	local_time_t timeToNextGateEnd;
};

struct Pulser{
	Pulser(){
		chargePulseFactor = 10;
		timeSinceLastPulse = 0;
	}
	uint32_t chargePulseFactor;
	local_time_t timeSinceLastPulse;

};

struct Constraint{
	std::string name;
	local_time_t *a;
	local_time_t b;
	constriant_type_t type;
};


struct Job{
	Job(){
		constraints.reserve(JOBSOLVER_MAX_NUM_CONSTRAINTS);
		name = "Undefined";
		priority = 0;
		rxChannel = 0;
		txChannel = 0;
		timeSinceLastExecuted = 1000;
		timeUntilJobReady = 0;
		timeUntilGateStart = 0;
		timeUntilGateEnd = 0;
		timeUntilPulseStart = 0;
		timeUntilPulseEnd = 0;
		timeUntilNextJobStart = 0;
		limitingConstraint = NULL;
	}
	std::string name;
	uint32_t priority;
	uint32_t rxChannel;
	uint32_t txChannel;
	local_time_t timeUntilGateStart;
	local_time_t timeUntilGateEnd;
	local_time_t timeUntilPulseStart;
	local_time_t timeUntilPulseEnd;
	local_time_t timeUntilNextJobStart;
	std::vector<Constraint> constraints;
	Constraint *limitingConstraint;
	local_time_t timeSinceLastExecuted;
	local_time_t timeUntilJobReady;
};



struct BaseBoard{
	BaseBoard(){
		Channel chan;
		channels.reserve(JOBSOLVER_BASEBOARD_NUM_CHAN);
		for(int i = 0; i < JOBSOLVER_BASEBOARD_NUM_CHAN; i++){
			channels.push_back(chan);
		}

		/* Add channel timing parameters */
		for(Channel& chan : channels){
			timedIncParameters.push_back(&chan.timeSinceLastPulse);
		}

		/* Add pulser timing parameters */
		timedIncParameters.push_back(&pulser.timeSinceLastPulse);

		/* Add adc timing parameters */
		timedIncParameters.push_back(&adc.timeSinceLastGate);

	}
	Pulser pulser;
	Adc adc;
	std::vector<Channel> channels;
	std::vector<local_time_t *>timedIncParameters;
	std::vector<local_time_t *>timedDecParameters;
} baseBoard;

struct System{
	System(){
		jobs.reserve(JOBSOLVER_PREALLOCATED_JOBS);
		systemTime = 0;
	}
	std::vector<Job*> jobs;
	system_time_t systemTime;
} system;

void addMinTimeSinceLastPulseOnChannelConstraint(Job *job, Channel *channel, local_time_t value){
	Constraint con;
	con.name = "MinTimeSinceLastPulseOnChannelConstraint";
	con.a = &channel->timeSinceLastPulse;
	con.type = gth;
	con.b = value;
	job->constraints.push_back(con);
}


/* Use this to ensure that the pulser has recovered charge enough from previous pulse */
void addMinTimeSinceLastPulseConstraint(Job *job, Pulser *pulser, local_time_t value){
	Constraint con;
	con.name = "MinTimeSinceLastPulse";
	con.a = &pulser->timeSinceLastPulse;
	con.type = gth;
	con.b = value;
	job->constraints.push_back(con);
}



/* Adds the constraint that enforces the min time between when the job is executed, i.e the job interval */
void addMinTimeSinceLastExecutedConstraint(Job *job, local_time_t value){
	Constraint con;
	con.name = "MinTimeSinceLastExecutedConstraint";
	con.a = &job->timeSinceLastExecuted;
	con.type = gth;
	con.b = value;
	job->constraints.push_back(con);
}

/* Adds a constraint the says the Adc has to be ready after */
void addAdcReadyAtTimeAfterPulseStart(Job *job, Adc *adc, local_time_t timeAfterPulseStart){
	Constraint con;
	con.name = "AdcReadyAtTimeAfterPulseStart";
	con.a = &adc->timeToNextGateEnd;
	con.type = lth;
	con.b = timeAfterPulseStart;
	job->constraints.push_back(con);
}


void printJobConstraints(Job *job){

	printf("JobConstraints:\r\n");
	for(Constraint con : job->constraints){
		printf("%s : a=%d, b=%d\r\n", con.name.c_str(), *con.a, con.b);

	}
	printf("\r\n");
}

int32_t getTimeUntilConstraintObeyed(Constraint *con){
	int32_t ret;
	switch(con->type){
	case lth:
		ret = *con->a - con->b;
		break;

	case gth:
		ret = con->b - *con->a;
		break;
	}
	return ret;
}


/* Update system parameters to the point in time at which
 * the job starts. */
void updateSystemTimingParametersToJobStart(Job *job){
	/* Update each parameter afflicted by job */
	baseBoard.adc.timeSinceLastGate = -job->timeUntilGateEnd;
	baseBoard.adc.timeToNextGateEnd = job->timeUntilGateEnd;
	baseBoard.channels[job->txChannel].timeSinceLastPulse = 0;
	baseBoard.pulser.timeSinceLastPulse = 0;
	job->timeSinceLastExecuted = 0;
}


void updateSystemTimingParametersWithTime(local_time_t time){

	/* Update all parameters with new time */
	for(local_time_t* param : baseBoard.timedIncParameters){
		*param += time;
	}

	for(local_time_t* param : baseBoard.timedDecParameters){
		*param -= time;
	}

	for(Job* job : system.jobs){
		job->timeSinceLastExecuted += time;
	}

}

void addNextJobToQueue(){
	Job *nextJob = NULL;
	int32_t timeUntilJobReady;
	int32_t timeUntilConReady;

	/* Iterate through possible jobs */
	for(Job* job : system.jobs){
		timeUntilJobReady = INT_MIN / 2;
		/* Determine based on current time, how long until job will be ready */

		/* Iterate through constraints */
		for(Constraint& con : job->constraints){
			/* Determine how long until constraint is fulfilled */
			timeUntilConReady = getTimeUntilConstraintObeyed(&con);

			if(timeUntilConReady > timeUntilJobReady){
				timeUntilJobReady = timeUntilConReady;
				job->limitingConstraint = &con;
			}
		}

		job->timeUntilJobReady = timeUntilJobReady;

		//printf("Time until job: %s is ready: %d\r\n", job->name.c_str(), job->timeUntilJobReady);

		/* If no other job is currently selected, use the first one */
		if(nextJob == NULL){
			nextJob = job;
		} else if(job->timeUntilJobReady < nextJob->timeUntilJobReady) {
			nextJob = job;
		} else if(job->timeUntilJobReady == nextJob->timeUntilJobReady) {
			/* If they are ready at the same time, the job with higher priority wins */
			if(job->priority > nextJob->priority){
				nextJob = job;
			}
		}
	}


	/* Update resources to reflect the added job */
	updateSystemTimingParametersWithTime(nextJob->timeUntilJobReady);

	system.systemTime += nextJob->timeUntilJobReady;
#if 1
	fprintf(pFile, "%s;%04d;%04d;%04d;%04d\r\n",
			nextJob->name.c_str(),
			(int32_t)system.systemTime + nextJob->timeUntilPulseStart,
			(int32_t)system.systemTime + nextJob->timeUntilPulseEnd,
			(int32_t)system.systemTime + nextJob->timeUntilGateStart,
			(int32_t)system.systemTime + nextJob->timeUntilGateEnd
	);
#endif
#if 1
	printf("%s;%08d;%08d;%08d;%08d\r\n",
			nextJob->name.c_str(),
			(int32_t)system.systemTime + nextJob->timeUntilPulseStart,
			(int32_t)system.systemTime + nextJob->timeUntilPulseEnd,
			(int32_t)system.systemTime + nextJob->timeUntilGateStart,
			(int32_t)system.systemTime + nextJob->timeUntilGateEnd
	);
	fflush(stdout);
#endif
	updateSystemTimingParametersToJobStart(nextJob);



}


void printSystemJobs(){
	for(Job* job : system.jobs){
		printf("%s\r\n", job->name.c_str());
	}
}

void addJobToSystemList(Job *job){
	system.jobs.push_back(job);
}


void rstJob(Job *job){
	job->name = "undefined";
	job->constraints.clear();
}


/* Calculate min. pulser chargetime required to generate some pulse. This function should
 * be expanded to take into account both pulse type, pulse Voltage. */
local_time_t getPulserChargeTimeRequiered(Job *job){
	return (local_time_t)((job->timeUntilPulseEnd - job->timeUntilPulseStart) * baseBoard.pulser.chargePulseFactor);
}
#define TIME_UNTIL_GATE_START_US 10.0
#define TIME_UNTIL_GATE_END_US 90.0
#define TIME_UNTIL_PULSE_START_US 0.0
#define TIME_UNTIL_PULSE_END_US 1.0
#define TIME_UNTIL_NEXT_JOB_START_US 10000


#define MIN_TIME_SINCE_LAST_PULSE_ON_SAME_CHANNEL 2000.0
#define MIN_TIME_SINCE_LAST_PULSE_DELTA 100.0


int main(){
	Job vJob, xJob, yJob, zJob;

	vJob.name = "V";
	vJob.priority = 1;
	vJob.rxChannel = 0;
	vJob.txChannel = 0;
	vJob.timeUntilGateStart = us_to_cc(TIME_UNTIL_GATE_START_US);
	vJob.timeUntilGateEnd = us_to_cc(TIME_UNTIL_GATE_END_US);
	vJob.timeUntilPulseStart = us_to_cc(TIME_UNTIL_PULSE_START_US);
	vJob.timeUntilPulseEnd = us_to_cc(TIME_UNTIL_PULSE_END_US);
	vJob.timeUntilNextJobStart = us_to_cc(TIME_UNTIL_NEXT_JOB_START_US);


	addMinTimeSinceLastPulseOnChannelConstraint(&vJob, &baseBoard.channels[0], us_to_cc(MIN_TIME_SINCE_LAST_PULSE_ON_SAME_CHANNEL));
	addMinTimeSinceLastPulseOnChannelConstraint(&vJob, &baseBoard.channels[1], us_to_cc(MIN_TIME_SINCE_LAST_PULSE_ON_SAME_CHANNEL-(1 * MIN_TIME_SINCE_LAST_PULSE_DELTA)));
	addMinTimeSinceLastPulseOnChannelConstraint(&vJob, &baseBoard.channels[2], us_to_cc(MIN_TIME_SINCE_LAST_PULSE_ON_SAME_CHANNEL-(2 * MIN_TIME_SINCE_LAST_PULSE_DELTA)));
	addMinTimeSinceLastPulseOnChannelConstraint(&vJob, &baseBoard.channels[3], us_to_cc(MIN_TIME_SINCE_LAST_PULSE_ON_SAME_CHANNEL-(3 * MIN_TIME_SINCE_LAST_PULSE_DELTA)));
	addMinTimeSinceLastPulseConstraint(&vJob, &baseBoard.pulser, getPulserChargeTimeRequiered(&vJob));
	addMinTimeSinceLastExecutedConstraint(&vJob, vJob.timeUntilNextJobStart);
	addAdcReadyAtTimeAfterPulseStart(&vJob, &baseBoard.adc, vJob.timeUntilGateStart);
	printJobConstraints(&vJob);
	addJobToSystemList(&vJob);
#if 1
	xJob.name = "X";
	xJob.priority = 1;
	xJob.rxChannel = 1;
	xJob.txChannel = 1;
	xJob.timeUntilGateStart = us_to_cc(TIME_UNTIL_GATE_START_US);
	xJob.timeUntilGateEnd = us_to_cc(TIME_UNTIL_GATE_END_US);
	xJob.timeUntilPulseStart = us_to_cc(TIME_UNTIL_PULSE_START_US);
	xJob.timeUntilPulseEnd = us_to_cc(TIME_UNTIL_PULSE_END_US);
	xJob.timeUntilNextJobStart = us_to_cc(TIME_UNTIL_NEXT_JOB_START_US);

	addMinTimeSinceLastPulseOnChannelConstraint(&xJob, &baseBoard.channels[0], us_to_cc(MIN_TIME_SINCE_LAST_PULSE_ON_SAME_CHANNEL-(1 * MIN_TIME_SINCE_LAST_PULSE_DELTA)));
	addMinTimeSinceLastPulseOnChannelConstraint(&xJob, &baseBoard.channels[1], us_to_cc(MIN_TIME_SINCE_LAST_PULSE_ON_SAME_CHANNEL));
	addMinTimeSinceLastPulseOnChannelConstraint(&xJob, &baseBoard.channels[2], us_to_cc(MIN_TIME_SINCE_LAST_PULSE_ON_SAME_CHANNEL-(1 * MIN_TIME_SINCE_LAST_PULSE_DELTA)));
	addMinTimeSinceLastPulseOnChannelConstraint(&xJob, &baseBoard.channels[3], us_to_cc(MIN_TIME_SINCE_LAST_PULSE_ON_SAME_CHANNEL-(2 * MIN_TIME_SINCE_LAST_PULSE_DELTA)));
	addMinTimeSinceLastPulseConstraint(&xJob, &baseBoard.pulser, getPulserChargeTimeRequiered(&xJob));
	addMinTimeSinceLastExecutedConstraint(&xJob, xJob.timeUntilNextJobStart);
	addAdcReadyAtTimeAfterPulseStart(&xJob, &baseBoard.adc, xJob.timeUntilGateStart);
	printJobConstraints(&xJob);
	addJobToSystemList(&xJob);

	yJob.name = "Y";
	yJob.priority = 1;
	yJob.rxChannel = 2;
	yJob.txChannel = 2;
	yJob.timeUntilGateStart = us_to_cc(TIME_UNTIL_GATE_START_US);
	yJob.timeUntilGateEnd = us_to_cc(TIME_UNTIL_GATE_END_US);
	yJob.timeUntilPulseStart = us_to_cc(TIME_UNTIL_PULSE_START_US);
	yJob.timeUntilPulseEnd = us_to_cc(TIME_UNTIL_PULSE_END_US);
	yJob.timeUntilNextJobStart = us_to_cc(TIME_UNTIL_NEXT_JOB_START_US);

	addMinTimeSinceLastPulseOnChannelConstraint(&yJob, &baseBoard.channels[0], us_to_cc(MIN_TIME_SINCE_LAST_PULSE_ON_SAME_CHANNEL-(2 * MIN_TIME_SINCE_LAST_PULSE_DELTA)));
	addMinTimeSinceLastPulseOnChannelConstraint(&yJob, &baseBoard.channels[1], us_to_cc(MIN_TIME_SINCE_LAST_PULSE_ON_SAME_CHANNEL-(1 * MIN_TIME_SINCE_LAST_PULSE_DELTA)));
	addMinTimeSinceLastPulseOnChannelConstraint(&yJob, &baseBoard.channels[2], us_to_cc(MIN_TIME_SINCE_LAST_PULSE_ON_SAME_CHANNEL));
	addMinTimeSinceLastPulseOnChannelConstraint(&yJob, &baseBoard.channels[3], us_to_cc(MIN_TIME_SINCE_LAST_PULSE_ON_SAME_CHANNEL-(1 * MIN_TIME_SINCE_LAST_PULSE_DELTA)));
	addMinTimeSinceLastPulseConstraint(&yJob, &baseBoard.pulser, getPulserChargeTimeRequiered(&yJob));
	addMinTimeSinceLastExecutedConstraint(&yJob, yJob.timeUntilNextJobStart);
	addAdcReadyAtTimeAfterPulseStart(&yJob, &baseBoard.adc, yJob.timeUntilGateStart);
	printJobConstraints(&yJob);
	addJobToSystemList(&yJob);


	zJob.name = "Z";
	zJob.priority = 1;
	zJob.rxChannel = 3;
	zJob.txChannel = 3;
	zJob.timeUntilGateStart = us_to_cc(TIME_UNTIL_GATE_START_US);
	zJob.timeUntilGateEnd = us_to_cc(TIME_UNTIL_GATE_END_US);
	zJob.timeUntilPulseStart = us_to_cc(TIME_UNTIL_PULSE_START_US);
	zJob.timeUntilPulseEnd = us_to_cc(TIME_UNTIL_PULSE_END_US);
	zJob.timeUntilNextJobStart = us_to_cc(TIME_UNTIL_NEXT_JOB_START_US);

	addMinTimeSinceLastPulseOnChannelConstraint(&zJob, &baseBoard.channels[0], us_to_cc(MIN_TIME_SINCE_LAST_PULSE_ON_SAME_CHANNEL-(3 * MIN_TIME_SINCE_LAST_PULSE_DELTA)));
	addMinTimeSinceLastPulseOnChannelConstraint(&zJob, &baseBoard.channels[1], us_to_cc(MIN_TIME_SINCE_LAST_PULSE_ON_SAME_CHANNEL-(2 * MIN_TIME_SINCE_LAST_PULSE_DELTA)));
	addMinTimeSinceLastPulseOnChannelConstraint(&zJob, &baseBoard.channels[2], us_to_cc(MIN_TIME_SINCE_LAST_PULSE_ON_SAME_CHANNEL-(1 * MIN_TIME_SINCE_LAST_PULSE_DELTA)));
	addMinTimeSinceLastPulseOnChannelConstraint(&zJob, &baseBoard.channels[3], us_to_cc(MIN_TIME_SINCE_LAST_PULSE_ON_SAME_CHANNEL));
	addMinTimeSinceLastPulseConstraint(&zJob, &baseBoard.pulser, getPulserChargeTimeRequiered(&zJob));
	addMinTimeSinceLastExecutedConstraint(&zJob, zJob.timeUntilNextJobStart);
	addAdcReadyAtTimeAfterPulseStart(&zJob, &baseBoard.adc, zJob.timeUntilGateStart);
	printJobConstraints(&zJob);
	addJobToSystemList(&zJob);

#endif

	printf("Job solver\r\n");
#if 0
	xJob.name = "X";
	xJob.priority = 1;
	xJob.rxChannel = 0;
	xJob.txChannel = 0;
	xJob.timeUntilGateStart = us_to_cc(10);
	xJob.timeUntilGateEnd = us_to_cc(90);
	xJob.timeUntilPulseStart = us_to_cc(0);
	xJob.timeUntilPulseEnd = us_to_cc(1);
	xJob.timeUntilNextJobStart = us_to_cc(2000);

	addMinTimeSinceLastPulseOnChannelConstraint(&xJob, &baseBoard.channels[0], us_to_cc(1000));
	addMinTimeSinceLastPulseOnChannelConstraint(&xJob, &baseBoard.channels[1], us_to_cc(100));	/* Own channel */
	addMinTimeSinceLastPulseConstraint(&xJob, &baseBoard.pulser, getPulserChargeTimeRequiered(&xJob));
	addMinTimeSinceLastExecutedConstraint(&xJob, xJob.timeUntilNextJobStart);
	addAdcReadyAtTimeAfterPulseStart(&xJob, &baseBoard.adc, xJob.timeUntilGateStart);
	printJobConstraints(&xJob);
	addJobToSystemList(&xJob);
#endif

#if 0
	yJob.name = "Y";
	yJob.priority = 2;
	yJob.rxChannel = 1;
	yJob.txChannel = 1;
	yJob.timeUntilGateStart = us_to_cc(20);
	yJob.timeUntilGateEnd = us_to_cc(100);
	yJob.timeUntilPulseStart = us_to_cc(0);
	yJob.timeUntilPulseEnd = us_to_cc(10);
	yJob.timeUntilNextJobStart = us_to_cc(800);

	addMinTimeSinceLastPulseOnChannelConstraint(&yJob, &baseBoard.channels[0], us_to_cc(40));
	addMinTimeSinceLastPulseOnChannelConstraint(&yJob, &baseBoard.channels[1], us_to_cc(100));
	addMinTimeSinceLastPulseOnChannelConstraint(&yJob, &baseBoard.channels[2], us_to_cc(100));
	addMinTimeSinceLastPulseConstraint(&xJob, &baseBoard.pulser, getPulserChargeTimeRequiered(&yJob));
	addMinTimeSinceLastExecutedConstraint(&yJob,  yJob.timeUntilNextJobStart);
	addAdcReadyAtTimeAfterPulseStart(&yJob, &baseBoard.adc, yJob.timeUntilGateStart);
	printJobConstraints(&yJob);
	addJobToSystemList(&yJob);
#endif

#if 0
	zJob.name = "Z";
	zJob.priority = 3;
	zJob.rxChannel = 1;
	zJob.txChannel = 1;
	zJob.timeUntilGateStart = us_to_cc(20);
	zJob.timeUntilGateEnd = us_to_cc(100);
	zJob.timeUntilPulseStart = us_to_cc(0);
	zJob.timeUntilPulseEnd = us_to_cc(10);
	zJob.timeUntilNextJobStart = us_to_cc(800);

	addMinTimeSinceLastPulseOnChannelConstraint(&zJob, &baseBoard.channels[0], us_to_cc(40));
	addMinTimeSinceLastPulseOnChannelConstraint(&zJob, &baseBoard.channels[1], us_to_cc(100));
	addMinTimeSinceLastPulseConstraint(&xJob, &baseBoard.pulser, getPulserChargeTimeRequiered(&zJob));
	addMinTimeSinceLastExecutedConstraint(&zJob,  zJob.timeUntilNextJobStart);
	addAdcReadyAtTimeAfterPulseStart(&zJob, &baseBoard.adc, zJob.timeUntilGateStart);
	printJobConstraints(&zJob);
	addJobToSystemList(&zJob);
#endif



	printSystemJobs();

	pFile = fopen("C:/temp/myfile.txt","w");

	for(int i = 0; i < 100; i++){
		addNextJobToQueue();
		fflush(stdout);
	}

	fclose(pFile);
//

	fflush(stdout);
}
